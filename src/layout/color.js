module.exports = class Color {
    static byName() {
        return new Proxy({ }, STYLES_HANDLER);
    }
};

const COLOR_NAMES = [
    'Black',
    'Red',
    'Green',
    'Yellow',
    'Blue',
    'Purple',
    'Cyan',
    'Gray'
];

const EFFECTS = { 'bold': 1, 'dim': 2, 'under': 4 };

const ELEMENTS = [ 'weight', 'under', 'foreground', 'background' ];

const foreground = isLight => isLight ? 90 : 30;

const background = isLight => isLight ? 100 : 40;

const STYLES_HANDLER = {
    get: function(target, property) {
        if (property === 'value')
            return makeControlStrings(target, property);

        if (property === 'reset')
            return ['\x1b[0m'];

        const nextTarget = Object.assign({ }, target);

        if (saveColor(nextTarget, property))
            return new Proxy(Object.assign(nextTarget, target), STYLES_HANDLER);

        if (saveEffect(nextTarget, property))
            return new Proxy(Object.assign(nextTarget, target), STYLES_HANDLER);
    }
};

function saveColor(target, property) {
    const lightRegexp = /Light(\w+)/g;

    let match = /fg(\w+)/g.exec(property);

    if (match) {
        const [,compositeColor] = match;
        match = lightRegexp.exec(compositeColor);
        const isLight = match !== null;
        const [,color = compositeColor] = match || [];
        target.foreground = foreground(isLight) + COLOR_NAMES.indexOf(color);
        return true;
    }

    match = /bg(\w+)/g.exec(property);

    if (match) {
        const [,compositeColor] = match;
        match = lightRegexp.exec(compositeColor);
        const isLight = match !== null;
        const [,color = compositeColor] = match || [];
        target.background = background(isLight) + COLOR_NAMES.indexOf(color);
        return true;
    }

    return false;
}

function saveEffect(target, property) {
    if (/^bold|dim$/.test(property)) {
        target.weight = EFFECTS[property];
        return true;
    }
    if (property === 'under') {
        target.under = EFFECTS.under;
        return true;
    }
    return false;
}

function makeControlStrings(target) {
    const append = (a, e) => {
        if (e in target) a.push(target[e]);
        return a;
    };
    const parts = ELEMENTS.reduce(append, []);
    if (parts.length)
        return [`\x1b[${parts.join(';')}m`, '\x1b[0m'];
}

