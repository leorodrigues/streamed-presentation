module.exports = class StylizedElement {
    setStyle(givenStyle) {
        const style = { };
        const keys = Object.keys(givenStyle);
        if (keys.length === 0)
            return;
        for (const name of keys)
            style[name] = givenStyle[name];
        this.style = style;
    }
};