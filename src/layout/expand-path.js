
class ExpandPath {

    *execute(path) {
        for (const p of path)
            for (const e of p.split('.'))
                yield e;
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new ExpandPath();

module.exports = ExpandPath;