const ElementFabricationError = require('./element-fabrication-error');
const VenetianBlade = require('./venetian-blade');
const Structure = require('./structure');
const Venetian = require('./venetian');
const Label = require('./label');
const Line = require('./line');
const Cell = require('./cell');

const MSG_CELL_NAME_REQUIRED = 'Cell name is a required parameter';

const MSG_LABEL_NAME_REQUIRED = 'Label name is a required paramenter';

class ElementFactory {

    newCell(name, style) {
        if (!name) throw new ElementFabricationError(MSG_CELL_NAME_REQUIRED);
        return new Cell(name, style);
    }

    newLabel(name, value, style) {
        if (!name) throw new ElementFabricationError(MSG_LABEL_NAME_REQUIRED);
        return new Label(name, value, style);
    }

    /* istanbul ignore next */
    newVenetian(openSwitch, blades, orientation) {
        return new Venetian(openSwitch, blades, orientation);
    }

    /* istanbul ignore next */
    newVenetianBlade(contents, condition) {
        return new VenetianBlade(contents, condition);
    }

    /* istanbul ignore next */
    newLine(iterator, contents) {
        return new Line(iterator, contents);
    }

    /* istanbul ignore next */
    newStructure(lines) {
        return new Structure(lines);
    }

    /* istanbul ignore next */
    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new ElementFactory();

module.exports = ElementFactory;
