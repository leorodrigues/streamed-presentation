const ConfigurationError = require('./configuration-error');

const FEATURES = [ 'width', 'color', 'padding', 'fill', 'align' ];

const AddFeature = require('./add-feature');

const ExpandPath = require('./expand-path');

const MSG_ERROR = 'Unable to load style';

module.exports = class Appearance {

    constructor(configuration,

        /* istanbul ignore next */
        addFeature = AddFeature.INSTANCE,

        /* istanbul ignore next */
        expandPath = ExpandPath.INSTANCE) {

        this.configuration = configuration;
        this.addFeature = addFeature;
        this.expandPath = expandPath;
    }

    getStyle(...path) {
        let configuration = this.configuration;
        for (const p of this.expandPath.execute(path)) {
            configuration = configuration[p];
            if (!configuration)
                throw new ConfigurationError(`${MSG_ERROR}: ${path.join('.')}`);
        }
        return FEATURES.reduce(this.addFeature.execute(configuration), { });
    }
};
