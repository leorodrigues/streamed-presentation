
class AddFeature {
    execute(configuration) {
        return (style, feature) => {
            const value = configuration[feature];
            if (value !== undefined) style[feature] = value;
            return style;
        };
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new AddFeature();

module.exports = AddFeature;