const AcceptVisitor = require('./accept-visitor');

class Structure {
    constructor(elements, acceptVisitor) {

        /* istanbul ignore next */
        elements = elements || [ ];

        /* istanbul ignore next */
        acceptVisitor = acceptVisitor || AcceptVisitor.INSTANCE;

        Object.assign(this, { elements, acceptVisitor });
    }

    accept(context, visitor) {
        const elements = this.elements;
        const acceptVisitor = this.acceptVisitor;
        acceptVisitor.execute(this, context, visitor, 'structure', elements);
        visitor.done();
    }
}

module.exports = Structure;