
const VenetianBlade = require('./venetian-blade');
const AcceptVisitor = require('./accept-visitor');

class Venetian {
    constructor(openSwitch, blades, orientation, acceptVisitor) {
        this.openSwitch = openSwitch;
        this.orientation = orientation;
        this.closedBlades = VenetianBlade.closedOnly(blades);
        this.openedBlades = VenetianBlade.openedOnly(blades);

        /* istanbul ignore next */
        this.acceptVisitor = acceptVisitor || AcceptVisitor.INSTANCE;
    }

    get pathPart() {
        return this.orientation;
    }

    accept(context, visitor) {
        const acceptVisitor = this.acceptVisitor;
        acceptVisitor.execute(
            this, context, visitor, 'venetian', this.openedBlades);
        acceptVisitor.execute(
            this, context, visitor, 'venetian', this.closedBlades);
    }
}

module.exports = Venetian;
