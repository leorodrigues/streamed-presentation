const StylizedElement = require('./stylized-element');

module.exports = class Label extends StylizedElement {
    constructor(name, value, style) {
        super();

        this.name = name;
        this.value = value;
        this.style = style;
    }

    get pathPart() {
        return `${this.name}.label`;
    }

    accept(context, visitor) {
        visitor.startVisit('label', this, context);
        visitor.endVisit('label', this, context);
    }
};
