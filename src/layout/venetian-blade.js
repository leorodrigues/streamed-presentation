const AcceptVisitor = require('./accept-visitor');

class VenetianBlade {
    constructor(contents, condition, acceptVisitor) {
        /* istanbul ignore next */
        this.condition = condition || VenetianBlade.CONDITION_OPENED;

        /* istanbul ignore next */
        this.contents = contents || [ ];

        /* istanbul ignore next */
        this.acceptVisitor = acceptVisitor || AcceptVisitor.INSTANCE;
    }

    /* istanbul ignore next */
    get pathPart() {
        return 'blade';
    }

    accept(context, visitor) {
        const contents = this.contents;
        this.acceptVisitor.execute(this, context, visitor, 'blade', contents);
    }

    static closedOnly(blades) {
        return blades.filter(b => b.condition === CONDITION_CLOSED);
    }

    static openedOnly(blades) {
        return blades.filter(b => b.condition === CONDITION_OPENED);
    }

    /* istanbul ignore next */
    static get CONDITION_OPENED() {
        return CONDITION_OPENED;
    }

    /* istanbul ignore next */
    static get CONDITION_CLOSED() {
        return CONDITION_CLOSED;
    }
}

const CONDITION_OPENED = 'opened';

const CONDITION_CLOSED = 'closed';

module.exports = VenetianBlade;
