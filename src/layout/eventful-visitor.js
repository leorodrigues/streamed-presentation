const Events = require('events');

class EventfulVisitor extends Events {
    startVisit(type, element, context) {
        this.emit(`enter-${type}`, element, context);
        return this;
    }

    endVisit(type, element, context) {
        this.emit(`exit-${type}`, element, context);
        return this;
    }

    done() {
        this.emit('done');
        return this;
    }
}

module.exports = EventfulVisitor;