const StylizedElement = require('./stylized-element');

module.exports = class Cell extends StylizedElement {
    constructor(name, style) {
        super();

        this.name = name;
        this.style = style;
    }

    get pathPart() {
        return `${this.name}.cell`;
    }

    accept(context, visitor) {
        visitor.startVisit('cell', this, context);
        visitor.endVisit('cell', this, context);
    }
};
