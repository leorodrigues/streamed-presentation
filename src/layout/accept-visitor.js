
class AcceptVisitor {
    execute(element, context, visitor, type, children) {
        visitor.startVisit(type, element, context);
        for (const e of children)
            e.accept(context, visitor);
        visitor.endVisit(type, element, context);
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new AcceptVisitor();

module.exports = AcceptVisitor;