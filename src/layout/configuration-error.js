/* istanbul ignore next */
class ConfigurationError extends Error { }

module.exports = ConfigurationError;