const AcceptVisitor = require('./accept-visitor');

module.exports = class Line {
    constructor(iterator, contents, acceptVisitor) {
        this.iterator = iterator;

        /* istanbul ignore next */
        this.contents = contents || [ ];

        /* istanbul ignore next */
        this.acceptVisitor = acceptVisitor || AcceptVisitor.INSTANCE;
    }

    /* istanbul ignore next */
    get pathPart() {
        return 'line';
    }

    accept(context, visitor) {
        const acceptVisitor = this.acceptVisitor;
        acceptVisitor.execute(this, context, visitor, 'line', this.contents);
    }
};
