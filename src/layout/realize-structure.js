const EventfulVisitor = require('./eventful-visitor');
const RealizationContext = require('./realization-context');

module.exports = class RealizeStructure {

    pushPath() {
        return (element, context) => {
            const part = element.pathPart;
            if (part)
                context.pushPath(part);
        };
    }

    popPath() {
        return (element, context) => {
            if (element.pathPart)
                context.popPath();
        };
    }

    stylizeElement() {
        return (element, context) => {
            const style = context.getStyle();
            element.setStyle(style);
        };
    }

    /* istanbul ignore next */
    makeContext(appearance) {
        return new RealizationContext(appearance);
    }

    makeDriver() {
        const stylizeElement = this.stylizeElement();
        const popPath = this.popPath();
        const pushPath = this.pushPath();

        const driver = new EventfulVisitor();

        driver.on('enter-label', pushPath);
        driver.on('exit-label', stylizeElement);
        driver.on('exit-label', popPath);

        driver.on('enter-cell', pushPath);
        driver.on('exit-cell', stylizeElement);
        driver.on('exit-cell', popPath);

        driver.on('enter-venetian', pushPath);
        driver.on('exit-venetian', popPath);

        driver.once('done', this.disposeOfHandlers(driver,
            'enter-venetian', 'exit-venetian',
            'enter-label', 'exit-label',
            'enter-cell', 'exit-cell'));

        return driver;
    }

    disposeOfHandlers(driver, ...eventNames) {
        return () => eventNames.forEach(n => driver.removeAllListeners(n));
    }
};
