class RealizationContext {
    constructor(appearance) {
        this.appearance = appearance;
        this.path = [ ];
    }

    pushPath(part) {
        this.path.push(part);
    }

    popPath() {
        this.path.pop();
    }

    getStyle() {
        return this.appearance.getStyle(...this.path);
    }
}

module.exports = RealizationContext;
