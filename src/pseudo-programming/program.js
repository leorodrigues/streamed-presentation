module.exports = class Program {
    constructor(operations) {
        return new Proxy(Object.assign(this, { operations }), HANDLER);
    }

    [Symbol.iterator]() {
        return this.operations[Symbol.iterator]();
    }

    get length() {
        return this.operations.length;
    }
};

const HANDLER = {
    get(target, property) {
        const parsed = parseInt(property.toString());
        return Number.isInteger(parsed)
            ? target.operations[parsed]
            : target[property];
    }
};