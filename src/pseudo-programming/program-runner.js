const ProgramIterator = require('./program-iterator');

module.exports = class ProgramRunner {
    constructor(
        contextFactory,

        /* istanbul ignore next */
        programIterator = ProgramIterator.INSTANCE) {

        this.contextFactory = contextFactory;
        this.programIterator = programIterator;
    }

    run(program, dataSource, dataOutput) {
        const context = this.contextFactory.newContext(dataSource, dataOutput);
        const operations = this.programIterator.iterate(program, context);
        for (const operation of operations) operation();
    }
};