const OperationSet = require('./operation-set');
const ProgramFactory = require('./program-factory');

class OperationSetBinder {
    constructor(
        /* istanbul ignore next */
        programFactory = ProgramFactory.INSTANCE,

        /* istanbul ignore next */
        operationSet = OperationSet.INSTANCE) {

        this.operationSet =  operationSet;
        this.programFactory = programFactory;
    }

    bind(pseudoCode) {
        const operations = [ ];
        const operationSet = this.operationSet;
        for (const { op, parameters } of pseudoCode)
            operations.push(operationSet[op](parameters));
        return this.programFactory.newProgram(operations);
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new OperationSetBinder();

module.exports = OperationSetBinder;