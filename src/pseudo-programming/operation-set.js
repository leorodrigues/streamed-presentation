class OperationSet {
    'emit'({ element }) {
        return context =>
            context.pushResult(context.convert(element));
    }

    'switch-iterator'({ name }) {
        return context =>
            context.switchIterator(name);
    }

    'fetch-data'() {
        return context =>
            context.fetchData();
    }

    'fetch-variable'({ name }) {
        return context =>
            context.fetchVariable(name);
    }

    'jump-exausted'({ offset }) {
        return (context, pointer) =>
            context.dataExausted && pointer.jumpOffset(offset);
    }

    'jump-offset'({ offset }) {
        return (context, pointer) =>
            pointer.jumpOffset(offset);
    }

    'jump-false'({ offset }) {
        return (context, pointer) =>
            context.isFalse && pointer.jumpOffset(offset);
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new OperationSet();

module.exports = OperationSet;