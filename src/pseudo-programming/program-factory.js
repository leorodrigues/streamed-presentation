const Program = require('./program');

/* istanbul ignore next: review this comment again on the next refactoring */
class ProgramFactory {
    newProgram(operations) {
        return new Program(operations);
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new ProgramFactory();

module.exports = ProgramFactory;