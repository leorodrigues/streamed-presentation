const ExecutionPointerFactory = require('./execution-pointer-factory');

class ProgramIterator {
    constructor(
        /* istanbul ignore next */
        pointerFactory = ExecutionPointerFactory.INSTANCE) {

        this.pointerFactory = pointerFactory;
    }

    *iterate(program, context) {
        const pointer = this.pointerFactory.newPointer(program.length);
        while (!pointer.endOfProgram) {
            const operation = program[pointer.position];
            yield () => operation(context, pointer);
            if (pointer.endOfProgram) break;
            pointer.stepForward();
        }
    }

    /* istanbul ignore next */
    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new ProgramIterator();

module.exports = ProgramIterator;