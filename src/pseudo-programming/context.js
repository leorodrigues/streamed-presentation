module.exports = class Context {
    constructor(dataSource, dataOutput, converter) {
        this.dataSource = dataSource;
        this.dataOutput = dataOutput;
        this.converter = converter;
        Object.assign(this, {
            converter,
            dataSource,
            dataOutput,
            dataObject: null,
            dataIterator: null
        });
    }

    get dataExausted() {
        return this.dataObject === null;
    }

    get isFalse() {
        return !this.currentValue;
    }

    pushResult(object) {
        this.dataOutput.push(object);
    }

    convert(element) {
        return this.converter.convert(element, this);
    }

    switchIterator(name) {
        this.dataIterator = this.dataSource.getIterator(name);
    }

    fetchData() {
        this.dataObject = this.dataIterator
            ? this.dataIterator.next().value || null
            : null;
    }

    fetchVariable(name) {
        this.currentValue = this.dataSource.getVariable(name) || null;
    }

    getDataField(name) {
        if (this.dataObject)
            return this.dataObject[name];
    }
};
