const ExecutionPointer = require('./execution-pointer');

/* istanbul ignore next: review this comment again on the next refactoring */
class ExecutionPointerFactory {
    newPointer(programLength) {
        return new ExecutionPointer(programLength);
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new ExecutionPointerFactory();

module.exports = ExecutionPointerFactory;