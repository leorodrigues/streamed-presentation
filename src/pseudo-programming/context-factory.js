const Context = require('./context');

/* istanbul ignore next: review this comment again on the next refactoring */
module.exports = class ContextFactory {
    constructor(converter) {
        this.converter = converter;
    }

    newContext(dataSource, dataOutput) {
        return new Context(dataSource, dataOutput, this.converter);
    }
};