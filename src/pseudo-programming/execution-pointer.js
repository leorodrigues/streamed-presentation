const ProgramJumpError = require('./program-jump-error');

module.exports = class ExecutionPointer {
    constructor( /* istanbul ignore next */ programLength = 0) {
        this.programLength = programLength;
        this.position = 0;
    }

    get endOfProgram() {
        return this.position >= this.programLength;
    }

    stepForward() {
        return this.jumpOffset(1);
    }

    jumpOffset(offset) {
        this.confirmDestinationIsWithinBounds(this.position + offset);
        const lastPosition = this.position;
        this.position += offset;
        return lastPosition;
    }

    confirmDestinationIsWithinBounds(destination) {
        if (destination > this.programLength)
            throw new ProgramJumpError('Offset is beyond end of program');
        if (destination < 0)
            throw new ProgramJumpError('Offset is before start of program');
    }
};