
const LayoutAssembler = require('./layout-assembler');

const ProgramStreamer = require('./program-streamer');

const { OperationSetBinder } = require('../pseudo-programming');

/* istanbul ignore next: review this comment at the next refactoring */
module.exports = class Presenter {
    constructor(layoutAssembler, operationSetBinder, programStreamer) {

        this.programStreamer = programStreamer
            || ProgramStreamer.INSTANCE;

        this.layoutAssembler = layoutAssembler
            || LayoutAssembler.INSTANCE;

        this.operationSetBinder = operationSetBinder
            || OperationSetBinder.INSTANCE;
    }

    assemblePseudoCode(structureData, appearanceData) {
        return this.layoutAssembler.assemble(structureData, appearanceData);
    }

    bindPseudoCodeIntoProgram(pseudoCode) {
        return this.operationSetBinder.bind(pseudoCode);
    }

    renderToStream(program, dataSource) {
        return this.programStreamer.stream(program, dataSource);
    }
};
