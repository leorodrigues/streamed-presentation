
const { PassThrough } = require('stream');

const { ContextFactory, Runner } = require('../pseudo-programming');

const DefaultConverter = require('./default-converter');

const FragmentRenderer = require('./fragment-renderer');

const CONTEXT_FACTORY = new ContextFactory(DefaultConverter.INSTANCE);

const PROGRAM_RUNNER = new Runner(CONTEXT_FACTORY);

class ProgramStreamer {
    constructor(programRunner, fragmentRenderer) {

        /* istanbul ignore next */
        this.programRunner = programRunner || PROGRAM_RUNNER;

        /* istanbul ignore next */
        this.fragmentRenderer = fragmentRenderer || FragmentRenderer.INSTANCE;
    }

    stream(program, dataSource) {
        const stream = new PassThrough({ objectMode: true });
        const resultStream = stream.pipe(this.fragmentRenderer.render());
        new Promise(() => {
            this.programRunner.run(program, dataSource, stream);
            stream.push(null);
        });
        return resultStream;
    }

    /* istanbul ignore next */
    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new ProgramStreamer();

module.exports = ProgramStreamer;