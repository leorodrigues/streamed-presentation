
const StructureTransformer = require('./structure-transformer');

class StructureBuilder {
    constructor(transformer = StructureTransformer.INSTANCE) {
        this.transformer = transformer;
    }

    buildFromData(data) {
        const driver = this.transformer.makeDriver();

        function follow(object) {
            const { type, ...element } = object;
            const children = [ ];
            const context = { children };
            driver.startVisit(type, element, context);

            const parameters = [ ];
            for (const child of children)
                parameters.push(follow(child));
            context.parameters = parameters;

            context.product = undefined;
            driver.endVisit(type, element, context);
            return context.product;
        }

        const result = follow(data);

        driver.done();

        return result;
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new StructureBuilder();

module.exports = StructureBuilder;