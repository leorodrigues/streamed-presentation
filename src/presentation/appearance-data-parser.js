const { GraphWeaver, HierarchyInspector } =
    require('@leorodrigues/hierarchy-inspector');

const INSPECTOR = new HierarchyInspector();

const DEFAULT_GRAPTH = new GraphWeaver()
    .atRoot()
    .addBranch('cell', 'width')
    .addBranch('cell', 'color')
    .addBranch('cell', 'padding')
    .addBranch('cell', 'fill')
    .addBranch('cell', 'align')
    .addBranch('label', 'width')
    .addBranch('label', 'color')
    .addBranch('label', 'padding')
    .addBranch('label', 'fill')
    .addBranch('label', 'align')
    .weave();

/* istanbul ignore next */
class AppearanceDataParser {
    constructor(graph = DEFAULT_GRAPTH, inspector = INSPECTOR) {
        Object.assign(this, { inspector, graph });
    }

    parse(jsonString) {
        return this.inspector.inspectBoundByGraph(
            JSON.parse(jsonString), this.graph);
    }
}

module.exports = AppearanceDataParser;