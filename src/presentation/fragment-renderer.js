const { Transform } = require('stream');

class FragmentRenderer {
    render() {
        return new Transform({
            writableObjectMode: true,
            transform(object, enc, callback) {
                this.push(fragmentToBuffer(object));
                callback();
            }
        });
    }

    stringify() {
        return new Transform({
            writableObjectMode: true,
            transform(object, enc, callback) {
                this.push(JSON.stringify(object) + '\n');
                callback();
            }
        });
    }

    /* istanbul ignore next */
    static get INSTANCE() {
        return INSTANCE;
    }
}

function fragmentToBuffer(fragment) {
    if (fragment.type === 'break')
        return Buffer.from('\n');
    const {
        padding = 1, fill = ' ', align = 'left', color = [ ], text = ''
    } = fragment;
    const colorWidth = color.reduce((s,S) => s + S.length, 0);
    const { width = text.length } = fragment;
    const buffer = Buffer.alloc(width + padding + colorWidth, fill);
    write(align, width, padding, buffer, text, color);
    return buffer;
}

function write(align, fieldLength, padding, buffer, text, color) {
    const contentLength = Math.min(text.length, fieldLength);
    let textOffset = computeOffset(contentLength, fieldLength, align);
    if (color.length) {
        const [start, end] = color;
        textOffset += buffer.write(start);
        textOffset += buffer.write(text, textOffset, contentLength);
        textOffset += buffer.write(end, fieldLength + start.length + padding);
        return textOffset;
    }
    return buffer.write(text, textOffset, fieldLength);
}

function computeOffset(length, width, align) {
    if (align === 'left') return 0;
    return width - length;
}

const INSTANCE = new FragmentRenderer();

module.exports = FragmentRenderer;