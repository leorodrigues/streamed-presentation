
const LayoutKit = require('./layout-kit');

class LayoutAssembler {
    constructor(layoutKit) {
        /* istanbul ignore next */
        this.layoutKit = layoutKit || LayoutKit.INSTANCE;
    }

    assemble(structureData, appearanceData) {
        const layoutKit = this.layoutKit;

        const appearance = layoutKit.newAppearance(appearanceData);
        const structure = layoutKit.newStructure(structureData);

        const realizeStructure = layoutKit.newRealizeStructure();
        const context = realizeStructure.makeContext(appearance);
        structure.accept(context, realizeStructure.makeDriver());

        const primedPseudoCode = structure.primeCode();
        return primedPseudoCode();
    }

    /* istanbul ignore next */
    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new LayoutAssembler();

module.exports = LayoutAssembler;
