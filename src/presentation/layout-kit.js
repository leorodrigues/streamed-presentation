const { Appearance, RealizeStructure } = require('../layout');
const StructureBuilder = require('./structure-builder');

/* istanbul ignore next */
class LayoutKit {
    constructor(structureBuilder = StructureBuilder.INSTANCE) {
        this.structureBuilder = structureBuilder;
    }

    newAppearance(appearanceData) {
        return new Appearance(appearanceData);
    }

    newStructure(structureData) {
        return this.structureBuilder.buildFromData(structureData);
    }

    newRealizeStructure() {
        return new RealizeStructure();
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new LayoutKit();

module.exports = LayoutKit;
