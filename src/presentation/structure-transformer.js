
const EventfulVisitor = require('../layout/eventful-visitor');
const { CompilableElementFactory } = require('../compilable-layout');

class StructureTransformer {

    constructor(factory = CompilableElementFactory.INSTANCE) {
        this.factory = factory;
    }

    handleEnterStructure() {
        return ({ contents }, { children }) =>
            children.push(...contents);
    }

    handleEnterLine() {
        return ({ contents }, { children }) =>
            children.push(...contents);
    }

    handleEnterVenetian() {
        return ({ blades }, { children }) => {
            for (const b of blades)
                children.push(Object.assign({ type: 'blade' }, b));
        };
    }

    handleEnterBlade() {
        return ({ contents }, { children }) =>
            children.push(...contents);
    }

    handleExitVenetian() {
        const factory = this.factory;
        return (element, ctx) => {
            const sw = element['opened-when'];
            const or = element['orientation'];
            ctx.product = factory.newVenetian(sw, ctx.parameters, or);
        };
    }

    handleExitBlade() {
        const factory = this.factory;
        return ({ when }, ctx) =>
            ctx.product = factory.newVenetianBlade(ctx.parameters, when);
    }

    handleExitStructure() {
        const factory = this.factory;
        return (element, ctx) =>
            ctx.product = factory.newStructure(ctx.parameters);
    }

    handleExitLine() {
        const factory = this.factory;
        return ({ iterator }, ctx) =>
            ctx.product = factory.newLine(iterator, ctx.parameters);
    }

    handleExitLabel() {
        const factory = this.factory;
        return ({ name, value }, ctx) =>
            ctx.product = factory.newLabel(name, value);
    }

    handleExitCell() {
        const factory = this.factory;
        return ({ name }, ctx) =>
            ctx.product = factory.newCell(name);
    }

    disposeOfHandlers(driver, ...eventNames) {
        return () => eventNames.forEach(n => driver.removeAllListeners(n));
    }

    makeDriver() {
        const driver = new EventfulVisitor();

        driver.on('enter-structure', this.handleEnterStructure());
        driver.on('enter-venetian', this.handleEnterVenetian());
        driver.on('enter-blade', this.handleEnterBlade());
        driver.on('enter-line', this.handleEnterLine());

        driver.on('exit-structure', this.handleExitStructure());
        driver.on('exit-venetian', this.handleExitVenetian());
        driver.on('exit-blade', this.handleExitBlade());
        driver.on('exit-label', this.handleExitLabel());
        driver.on('exit-cell', this.handleExitCell());
        driver.on('exit-line', this.handleExitLine());

        driver.once('done', this.disposeOfHandlers(
            driver, 'enter-structure', 'enter-line', 'exit-structure',
            'exit-cell', 'exit-label', 'exit-line'));

        return driver;
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new StructureTransformer();

module.exports = StructureTransformer;