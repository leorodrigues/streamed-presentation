const ConversionError = require('../pseudo-programming').ConversionError;

const BREAK = { type: 'break' };

class DefaultConverter {
    convert(elmt, context) {
        const type = elmt.type;
        if (type === 'label')
            return this.label(context, type, elmt.name, elmt.style, elmt.value);
        if (type === 'cell')
            return this.cell(context, type, elmt.name, elmt.style);
        if (type === 'break')
            return BREAK;
        throw new ConversionError(`Unknown element type "${type}"`);
    }

    label(context, type, name, style, value) {
        let text = value
            || context.getDataField(name)
            || name.toLocaleUpperCase();
        text = text.toString();
        return Object.assign({ type, text }, style);
    }

    cell(context, type, name, style) {
        let text = context.getDataField(name) || '';
        text = text.toString();
        return Object.assign({ type, text }, style);
    }

    static get INSTANCE() {
        return CONVERTER;
    }
}

const CONVERTER = new DefaultConverter();

module.exports = DefaultConverter;