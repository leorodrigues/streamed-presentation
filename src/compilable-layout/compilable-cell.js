
const PseudoCodeFactory = require('./pseudo-code-factory');
const { Cell } = require('../layout');

class CompilableCell extends Cell {
    constructor(name, style, pseudoCodeFactory) {
        super(name, style);

        /* istanbul ignore next */
        this.pseudoCodeFactory = pseudoCodeFactory
            || PseudoCodeFactory.INSTANCE;
    }

    primeCode(context) {
        const element = { type: 'cell', name: this.name };
        if (this.style)
            element.style = this.style;
        context.push(this.pseudoCodeFactory.primeEmitElement(element));
    }
}

module.exports = CompilableCell;
