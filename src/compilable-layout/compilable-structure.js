
const PseudoCodeFactory = require('./pseudo-code-factory');
const { Structure } = require('../layout');

class CompilableStructure extends Structure {
    constructor(elements, pseudoCodeFactory) {
        super(elements);

        /* istanbul ignore next */
        this.pseudoCodeFactory = pseudoCodeFactory
            || PseudoCodeFactory.INSTANCE;
    }

    primeCode(context) {
        /* istanbul ignore next */
        context = context || this.pseudoCodeFactory.newContext();
        for (const e of this.elements)
            e.primeCode(context);
        return () => context.map(p => p());
    }
}

module.exports = CompilableStructure;