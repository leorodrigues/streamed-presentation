
const PseudoCodeFactory = require('./pseudo-code-factory');
const { Label } = require('../layout');

class CompilableLabel extends Label {
    constructor(name, value, style, pseudoCodeFactory) {
        super(name, value, style);

        /* istanbul ignore next */
        this.pseudoCodeFactory = pseudoCodeFactory
            || PseudoCodeFactory.INSTANCE;
    }

    primeCode(context) {
        const element = { type: 'label', name: this.name };
        if (this.style)
            element.style = this.style;
        if (this.value)
            element.value = this.value;
        context.push(this.pseudoCodeFactory.primeEmitElement(element));
    }
}

module.exports = CompilableLabel;
