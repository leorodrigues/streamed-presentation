const { VenetianBlade } = require('../layout');

class CompilableVenetianBlade extends VenetianBlade {
    constructor(contents, condition) {
        super(contents, condition);
    }

    primeCode(context) {
        for (const c of this.contents)
            c.primeCode(context);
    }
}

module.exports = CompilableVenetianBlade;