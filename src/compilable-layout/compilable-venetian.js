
const PseudoCodeFactory = require('./pseudo-code-factory');
const { Venetian } = require('../layout');

class CompilableVenetian extends Venetian {
    constructor(openSwitch, blades, orientation, pseudoCodeFactory) {
        super(openSwitch, blades, orientation);

        /* istanbul ignore next */
        this.pseudoCodeFactory = pseudoCodeFactory
            || PseudoCodeFactory.INSTANCE;
    }

    primeCode(context) {
        const pseudoCodeFactory = this.pseudoCodeFactory;

        const skipOpenedBlades = context.newLabel();
        context.push(pseudoCodeFactory.primeFetchVariable(this.openSwitch));

        if (this.closedBlades.length) {
            const skipClosedBlades = context.newLabel();
            context.push(pseudoCodeFactory.primeJumpFalse(skipOpenedBlades));

            skipOpenedBlades.markStart();
            for (const b of this.openedBlades)
                b.primeCode(context);
            context.push(pseudoCodeFactory.primeJumpOffset(skipClosedBlades));
            skipOpenedBlades.markStop();

            skipClosedBlades.markStart();
            for (const b of this.closedBlades)
                b.primeCode(context);
            skipClosedBlades.markStop();
        } else {
            context.push(pseudoCodeFactory.primeJumpFalse(skipOpenedBlades));

            skipOpenedBlades.markStart();
            for (const b of this.openedBlades)
                b.primeCode(context);
            skipOpenedBlades.markStop();
        }
    }
}

module.exports = CompilableVenetian;
