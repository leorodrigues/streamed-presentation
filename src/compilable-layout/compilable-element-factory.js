const CompilableCell = require('./compilable-cell');
const CompilableLine = require('./compilable-line');
const CompilableLabel = require('./compilable-label');
const CompilableVenetian = require('./compilable-venetian');
const CompilableStructure = require('./compilable-structure');
const CompilableVenetianBlade = require('./compilable-venetian-blade');

/* istanbul ignore next: review this comment in the next refactoring */
class CompilableElementFactory {

    newLabel(name, value, style) {
        return new CompilableLabel(name, value, style);
    }

    newLine(iterator, contents) {
        return new CompilableLine(iterator, contents);
    }

    newCell(name, style) {
        return new CompilableCell(name, style);
    }

    newVenetian(openSwitch, blades, orientation) {
        return new CompilableVenetian(openSwitch, blades, orientation);
    }

    newVenetianBlade(contents, condition) {
        return new CompilableVenetianBlade(contents, condition);
    }

    newStructure(lines) {
        return new CompilableStructure(lines);
    }

    static get INSTANCE() {
        return INSTANCE;
    }
}

const INSTANCE = new CompilableElementFactory();

module.exports = CompilableElementFactory;