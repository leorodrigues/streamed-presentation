
class CompilationContext extends Array {
    /* istanbul ignore next */
    newLabel() {
        return new PseudocodeLabel(this);
    }

    /* istanbul ignore next */
    newReverseLabel() {
        return new PseudocodeLabel(this, PseudocodeLabel.REVERSE);
    }

    /* istanbul ignore next */
    static get PseudocodeLabel() {
        return PseudocodeLabel;
    }
}

class PseudocodeLabel {
    constructor(pseudocode, direction = PseudocodeLabel.FORWARD) {
        /* istanbul ignore next */
        this.direction = direction || PseudocodeLabel.FORWARD;
        this.pseudocode = pseudocode;
    }

    markStart() {
        this.start = this.pseudocode.length;
    }

    markStop(/* istanbul ignore next */ offset = 0) {
        this.stop = this.pseudocode.length + offset;
    }

    get offset() {
        return (this.stop - this.start) * this.direction;
    }

    static get REVERSE() {
        return -1;
    }

    static get FORWARD() {
        return 1;
    }
}

module.exports = CompilationContext;
