const PseudoCodeFactory = require('./pseudo-code-factory');
const { Line } = require('../layout');

class CompilableLine extends Line {
    constructor(iterator, contents, pseudoCodeFactory) {
        super(iterator, contents);

        /* istanbul ignore next */
        this.pseudoCodeFactory = pseudoCodeFactory
            || PseudoCodeFactory.INSTANCE;
    }

    primeCode(context) {
        const pseudoCodeFactory = this.pseudoCodeFactory;
        const iterator = this.iterator;

        if (iterator) {
            const skipLoop = context.newLabel();
            const repeatLoop = context.newReverseLabel();
            context.push(pseudoCodeFactory.primeSwitchIterator(iterator));
            context.push(pseudoCodeFactory.primeFetchData());
            context.push(pseudoCodeFactory.primeJumpExausted(skipLoop));
            skipLoop.markStart();
            repeatLoop.markStart();
            for (const c of this.contents)
                c.primeCode(context);
            context.push(pseudoCodeFactory.primeEmitLineBreak());
            context.push(pseudoCodeFactory.primeJumpOffset(repeatLoop));
            skipLoop.markStop();
            repeatLoop.markStop(2);
        } else {
            for (const c of this.contents)
                c.primeCode(context);
            context.push(pseudoCodeFactory.primeEmitLineBreak());
        }
    }
}

module.exports = CompilableLine;
