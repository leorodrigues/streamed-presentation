
const CompilationContext = require('./compilation-context');

class PseudoCodeFactory {
    /* istanbul ignore next */
    newContext() {
        return new CompilationContext();
    }

    primeEmitElement(element) {
        return () => {
            return { op: 'emit', parameters: { element } };
        };
    }

    primeJumpExausted(label) {
        return () => {
            const offset = label.offset;
            return { op: 'jump-exausted', parameters: { offset } };
        };
    }

    primeJumpOffset(label) {
        return () => {
            const offset = label.offset;
            return { op: 'jump-offset', parameters: { offset } };
        };
    }

    primeJumpFalse(label) {
        return () => {
            const offset = label.offset;
            return { op: 'jump-false', parameters: { offset } };
        };
    }

    primeFetchVariable(name) {
        return () => {
            return { op: 'fetch-variable', parameters: { name } };
        };
    }

    primeSwitchIterator(name) {
        return () => {
            return { op: 'switch-iterator', parameters: { name } };
        };
    }

    primeEmitLineBreak() {
        return () => EMIT_LINE_BREAK;
    }

    primeFetchData() {
        return () => FETCH_DATA;
    }

    /* istanbul ignore next */
    get EMIT_LINE_BREAK() {
        return EMIT_LINE_BREAK;
    }

    /* istanbul ignore next */
    get FETCH_DATA() {
        return FETCH_DATA;
    }

    /* istanbul ignore next */
    static get INSTANCE() {
        return INSTANCE;
    }
}

const EMIT_LINE_BREAK = {
    op: 'emit', parameters: { element: { type: 'break' } }
};

const FETCH_DATA = {
    op: 'fetch-data'
};

const INSTANCE = new PseudoCodeFactory();

module.exports = PseudoCodeFactory;
