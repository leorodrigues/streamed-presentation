module.exports = {
    ...require('./src/layout'),
    ...require('./src/presentation'),
    ...require('./src/compilable-layout'),
    ...require('./src/pseudo-programming')
};
