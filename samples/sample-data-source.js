
class SampleDataSource {
    constructor(dataSetMap, currentDataSet = 'people', status = 'on') {
        const iterators = { };
        for (const name in dataSetMap)
            iterators[name] = dataSetMap[name][Symbol.iterator]();
        this.iterators = iterators;
        this.currentDataSet = currentDataSet;
        this.status = status;
    }

    getIterator(name) {
        return this.iterators[name];
    }

    getVariable(name) {
        if (name === 'displayingPeople')
            return this.currentDataSet === 'people';
        if (name === 'displayingTransfers')
            return this.currentDataSet === 'transfers';
        return this.status === 'on';
    }
}

module.exports = SampleDataSource;