
const colors = require('../').Color.byName();

const personHeading = colors.bgBlue.fgWhite.value;
const transferHeading = colors.bgGreen.value;
const errorBadge = colors.bgGray.fgRed.value;
const infoBadge = colors.bgGray.fgBlack.value;
const defaultText = colors.fgGray.value;

module.exports = {
    'message': {
        'label': { 'width': 50, 'padding': 0, 'color': defaultText }
    },
    'empty-data-badge': {
        'label': { 'width': 4, 'padding': 0, 'color': infoBadge }
    },
    'error-badge': {
        'label': { 'width': 4, 'padding': 0, 'color': errorBadge }
    },
    'name': {
        'cell': { 'width': 20, 'padding': 1 },
        'label': { 'width': 20, 'padding': 0, 'color': personHeading }
    },
    'age': {
        'cell': { 'width': 5, 'padding': 0, 'align': 'right' },
        'label': { 'width': 5, 'color': personHeading }
    },
    'date': {
        'cell': { 'width': 10 },
        'label': { 'width': 10, 'color': transferHeading }
    },
    'currency': {
        'label': { 'width': 1, 'padding': 1, color: transferHeading }
    },
    'amount': {
        'cell': { 'width': 15, 'padding': 0, 'fill': '.', 'align': 'right' },
        'label': { 'width': 12, 'color': transferHeading, 'align': 'right' }
    }
};