const firstNames = [ 'Jack', 'John', 'Sally', 'Simmon', 'Mary', 'Ginna' ];

const surNames = [ 'Fields', 'Clark', 'Thompson' ];

const ages = [ 21, 25, 27, 30 ];

const people = [...function *y() {
    for (const f of firstNames)
        for (const s of surNames)
            yield {
                name: `${f} ${s}`,
                age: ages[Math.floor(Math.random() * Math.floor(ages.length))],
                index: Math.random()
            };
}()].sort((a, b) => b.index - a.index);

const transfers = [...function *x() {
    for (let i = 1; i <= 10; i++)
        yield {
            date: new Date('2019', 0, i).toISOString(),
            amount: (Math.random() * 10).toFixed(2)
        };
}()];

module.exports = { people, transfers };