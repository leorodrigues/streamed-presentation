// Add some switch functionality with which we could play.
const [,,currentDataSet, status] = process.argv;

// Import a tool to simplify the piping of streams
const { pipeline } = require('stream');

// Import the presenter class
const { Presenter } = require('../');

// Have the structure and appearance data ready
const structureData = require('./sample-structure');

const appearanceData = require('./sample-appearance');

// Instantiate a fully capable, default presenter
const presenter = new Presenter();

// Generate a pseudocode from structure and appearance data
const pseudocode = presenter.assemblePseudoCode(structureData, appearanceData);

// Bind the pseudocode to a the actual functions
const program = presenter.bindPseudoCodeIntoProgram(pseudocode);

// Load the data
const sampleData = require('./sample-data');

// Have a data source envelope the data.
//
// Data sources are specifically tailored to deal
// with the data they envelope. They bridge the gap
// between the actual presenter and the data objects
// they should present, by holding the logics and tests
// that the presenter lacks.
const DataSource = require('./sample-data-source');

const dataSource = new DataSource(sampleData, currentDataSet, status);

// Ask the presenter to render the data, so it can be piped through
// to the standard output
pipeline(
    presenter.renderToStream(program, dataSource),
    process.stdout,
    e => process.stdout.write(e));
