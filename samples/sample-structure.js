const fs = require('fs');
const yaml = require('yaml');
const content = fs.readFileSync(`${__dirname}/sample-structure.yaml`);

module.exports = yaml.parse(content.toString());