const readline = require('readline');
const yaml = require('yaml');

const lines = [ ];
const reader = readline.createInterface({
    input: process.stdin, terminal: false
});

reader.on('line', l => lines.push(l));
reader.on('close', () => {
    const contents = lines.join('\n');
    const instance = yaml.parse(contents);
    console.log(JSON.stringify(instance, null, 4));
});
