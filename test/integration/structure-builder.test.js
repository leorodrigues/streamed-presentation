const chai = require('chai');

const { expect } = chai;

const { StructureBuilder, LayoutKit } = require('../../src/presentation');

const structureData = require('./resources/structure-data.json');
const appearanceData = require('./resources/appearance-data.json');
const expectedPseudoCode = require('./resources/expected-pseudo-code.json');
const venetiansData = require('./resources/structure-with-venetians.json');
const expectedPseudoCodeVenetians =
    require('./resources/expected-pseudo-code-venetians.json');

const builder = StructureBuilder.INSTANCE;

describe('presentation/Structure', () => {
    describe('#primeCode', () => {
        it('Should compile code correctly without venetians', () => {
            const appearance = LayoutKit.INSTANCE.newAppearance(appearanceData);
            const realizeStructure = LayoutKit.INSTANCE.newRealizeStructure();

            const structure = builder.buildFromData(structureData);
            const context = realizeStructure.makeContext(appearance);
            structure.accept(context, realizeStructure.makeDriver());

            const primedPseudoCode = structure.primeCode();

            expect(primedPseudoCode()).to.be.deep.equal(expectedPseudoCode);
        });

        it('Should compile code correctly with venetians', () => {
            const structure = builder.buildFromData(venetiansData);

            const primedPseudoCode = structure.primeCode();

            expect(primedPseudoCode()).to.be.deep.equal(expectedPseudoCodeVenetians);
        });
    });
});
