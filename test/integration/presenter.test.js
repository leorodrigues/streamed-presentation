const { Writable, pipeline } = require('stream');
const chai = require('chai');

const { expect } = chai;

const { Presenter } = require('../../src/presentation');

const structureData = require('./resources/structure-data.json');
const appearanceData = require('./resources/appearance-data.json');
const expectedPseudoCode = require('./resources/expected-pseudo-code.json');

const presenter = new Presenter();

const data = {
    people: [ { name: 'Jack', age: 23 } ]
};

const dataSource = {
    getIterator(name) {
        return data[name][Symbol.iterator]();
    }
};

describe('presentation/Presenter', () => {
    it('Should assemble a layout', () => {
        const pseudo = presenter.assemblePseudoCode(structureData, appearanceData);
        expect(pseudo).to.be.deep.equal(expectedPseudoCode);
    });

    it('Should compile pseudo code', async () => {
        const output = [ ];
        const program = presenter.bindPseudoCodeIntoProgram(expectedPseudoCode);
        await (new Promise(resolve => {
            const readable = presenter.renderToStream(program, dataSource);
            const writable = makeWritable(output);
            pipeline(readable, writable, e => resolve(e));
        }));
        expect(output).to.be.deep.equal([
            'Name                 ', 'Age ', '\n',
            'Jack                 ', '23  ', '\n'
        ]);
    });
});

function makeWritable(output) {
    return new Writable({
        objectMode: true,
        write(chunk, env, cb) {
            output.push(chunk.toString());
            cb();
        }
    });
}