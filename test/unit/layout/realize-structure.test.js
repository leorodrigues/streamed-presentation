const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { RealizeStructure } = require('../../../src/layout');

const sandbox = sinon.createSandbox();

const setStyle = sandbox.stub();
const getStyle = sandbox.stub();
const pushPath = sandbox.stub();
const popPath = sandbox.stub();

const subject = new RealizeStructure();

describe('layout/RealizeStructure', () => {
    afterEach(() => sandbox.reset());

    it('Should respond to "enter-venetian" and append a path part', () => {
        const element = { name: 'the-element', pathPart: 'x' };
        const context = { pushPath };
        const driver = subject.makeDriver();
        driver.emit('enter-venetian', element, context);
        driver.emit('done');
        expect(pushPath).to.be.calledOnceWithExactly('x');
    });

    it('Should respond to "enter-venetian" and do nothing', () => {
        const element = { name: 'the-element' };
        const context = { pushPath };
        const driver = subject.makeDriver();
        driver.emit('enter-venetian', element, context);
        driver.emit('done');
        expect(pushPath).to.not.be.called;
    });

    it('Should respond to "exit-venetian" and remove a path part', () => {
        const element = { name: 'the-element', pathPart: 'x' };
        const context = { popPath };
        const driver = subject.makeDriver();
        driver.emit('exit-venetian', element, context);
        driver.emit('done');
        expect(popPath).to.be.calledOnceWithExactly();
    });

    it('Should respond to "exit-venetian" and do nothing', () => {
        const element = { name: 'the-element' };
        const context = { popPath };
        const driver = subject.makeDriver();
        driver.emit('exit-venetian', element, context);
        driver.emit('done');
        expect(popPath).to.not.be.called;
    });

    it('Should respond to "exit-label" and apply the style to the label', () => {
        const element = { setStyle, name: 'the-element' };
        const driver = subject.makeDriver();
        getStyle.returns('the-style');
        driver.emit('exit-label', element, { getStyle, pushPath, popPath });
        driver.emit('done');
        expect(getStyle).to.be.calledOnceWithExactly();
        expect(setStyle).to.be.calledOnceWithExactly('the-style');
    });

    it('Should respond to "exit-cell" and apply the style to the cell', () => {
        const element = { setStyle, name: 'the-element' };
        const driver = subject.makeDriver();
        getStyle.returns('the-style');
        driver.emit('exit-cell', element, { getStyle, pushPath, popPath });
        driver.emit('done');
        expect(getStyle).to.be.calledOnceWithExactly();
        expect(setStyle).to.be.calledOnceWithExactly('the-style');
    });
});