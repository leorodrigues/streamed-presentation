const chai = require('chai');

const { expect } = chai;

const { ElementFactory, Cell, Label, ElementFabricationError } =
    require('../../../src/layout');

const subject = new ElementFactory();

describe('layout/ElementFactory', () => {
    describe('#newCell', () => {
        it('Should fabricate a cell correctly', () => {
            const cell = subject.newCell('N', 'S');
            expect(cell).to.be.instanceOf(Cell);
            expect(cell).to.be.deep.equal({ name: 'N', style: 'S' });
        });

        it('Should throw an exception if the name is missing', () => {
            expect(() => subject.newCell(undefined, 'S'))
                .to.be.throw(ElementFabricationError);
        });
    });

    describe('#newLabel', () => {
        it('Should fabricate a label correctly', () => {
            const label = subject.newLabel('N', 'V', 'S');
            expect(label).to.be.instanceOf(Label);
            expect(label).to.be.deep.equal({ name: 'N', style: 'S', value: 'V' });
        });

        it('Should throw an exception if the name is missing', () => {
            expect(() => subject.newLabel(undefined, 'S'))
                .to.be.throw(ElementFabricationError);
        });
    });
});