const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { Label } = require('../../../src/layout');

const subject = new Label('testField', 'TEST FIELD', 'some-style');

const sandbox = sinon.createSandbox();

const startVisit = sandbox.stub();
const endVisit = sandbox.stub();

describe('layout/Label', () => {
    describe('#accept', () => {
        it('Should apply the given visitor onto itself', () => {
            subject.accept('C', { endVisit, startVisit });
            expect(startVisit).to.be.calledOnceWithExactly('label', subject, 'C');
            expect(endVisit).to.be.calledOnceWithExactly('label', subject, 'C');
        });
    });

    describe('#pathPart', () => {
        it('Should give out the orientation', () =>
            expect(new Label('nm', 'vl').pathPart).to.be.equal('nm.label'));
    });
});