
const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { VenetianBlade } = require('../../../src/layout');

const sandbox = sinon.createSandbox();

const execute = sandbox.stub();

const subject = new VenetianBlade('c', 'W', { execute });

describe('layout/VenetianBlade', () => {
    describe('#accept', () => {
        it('Should apply the given visitor onto itself', () => {
            subject.accept('C', 'V');
            expect(execute).to.be.calledOnceWithExactly(
                subject, 'C', 'V', 'blade', 'c');
        });
    });

    describe('.openedOnly', () => {
        it('Should filter out the "closed" blades', () => {
            const fakeBlades = [
                { condition: VenetianBlade.CONDITION_CLOSED },
                { condition: VenetianBlade.CONDITION_OPENED }
            ];
            expect(VenetianBlade.openedOnly(fakeBlades)).to.be.deep.equal([
                { condition: VenetianBlade.CONDITION_OPENED }
            ]);
        });
    });

    describe('.closedOnly', () => {
        it('Should filter out the "opened" blades', () => {
            const fakeBlades = [
                { condition: VenetianBlade.CONDITION_CLOSED },
                { condition: VenetianBlade.CONDITION_OPENED }
            ];
            expect(VenetianBlade.closedOnly(fakeBlades)).to.be.deep.equal([
                { condition: VenetianBlade.CONDITION_CLOSED }
            ]);
        });
    });
});
