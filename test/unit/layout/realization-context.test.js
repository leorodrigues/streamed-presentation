const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { RealizationContext } = require('../../../src/layout');

const sandbox = sinon.createSandbox();

const getStyle = sandbox.stub();

describe('layout/RealizationContext', () => {
    afterEach(() => sandbox.reset());

    describe('#pushPath', () => {
        it('Should increase the path by one part', () => {
            getStyle.returns('S');
            const subject = new RealizationContext({ getStyle });
            subject.pushPath('x');
            expect(subject.getStyle()).to.be.equal('S');
            expect(getStyle).to.be.calledOnceWithExactly('x');
        });
    });

    describe('#popPath', () => {
        it('Should decrease the path by one part', () => {
            getStyle.returns('S');
            const subject = new RealizationContext({ getStyle });
            subject.path = [ 'b', 'c' ];
            subject.popPath();
            expect(subject.getStyle()).to.be.equal('S');
            expect(getStyle).to.be.calledOnceWithExactly('b');
        });
    });
});