const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { Venetian, VenetianBlade } = require('../../../src/layout');

const { expect } = chai;

const sandbox = sinon.createSandbox();

const execute = sandbox.stub();

const fakeBlades = [
    { condition: VenetianBlade.CONDITION_OPENED },
    { condition: VenetianBlade.CONDITION_CLOSED }
];

const fakeVisitor = {
    execute
};

describe('layout/Venetian', () => {
    describe('#accept', () => {
        it('Should apply the visitor onto itself', () => {
            const subject = new Venetian('test', fakeBlades, 'o', fakeVisitor);

            subject.accept('C', 'V');

            expect(execute).to.be.calledTwice
                .and.calledWithExactly(subject, 'C', 'V', 'venetian', [
                    { condition: VenetianBlade.CONDITION_OPENED },
                ])
                .and.calledWithExactly(subject, 'C', 'V', 'venetian', [
                    { condition: VenetianBlade.CONDITION_OPENED },
                ]);

            expect(subject.openedBlades).to.be.deep.equals([
                { condition: VenetianBlade.CONDITION_OPENED }
            ]);

            expect(subject.closedBlades).to.be.deep.equals([
                { condition: VenetianBlade.CONDITION_CLOSED }
            ]);
        });
    });

    describe('#pathPart', () => {
        it('Should give out the orientation', () =>
            expect(new Venetian('sw', [ ], 'or').pathPart).to.be.equal('or'));
    });
});