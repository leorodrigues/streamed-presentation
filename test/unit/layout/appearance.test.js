const chai = require('chai');

const { expect } = chai;

const { Appearance, ConfigurationError } = require('../../../src/layout');

const subject = new Appearance({
    people: { name: { line: { cell: { color: 'green' } } } }
});

describe('layout/Appearance', () => {
    describe('#getStyle', () => {
        it('Should look up the configuration searching for a tyle', () => {
            const style = subject.getStyle('people', 'name', 'line', 'cell');
            expect(style).to.be.deep.equal({ color: 'green' });
        });

        it('Should throw if a configuration element is not found', () => {
            expect(() => subject.getStyle('unknown')).to.throw(ConfigurationError);
        });
    });
});