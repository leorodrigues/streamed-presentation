const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { Line } = require('../../../src/layout');

const sandbox = sinon.createSandbox();

const startVisit = sandbox.stub();
const endVisit = sandbox.stub();
const accept = sandbox.stub();

const subject = new Line('tableName', [{ accept }, { accept }]);

describe('layout/Line', () => {
    describe('#accept', () => {
        it('Should apply the given visitor to it\'s elements and onto itself', () => {
            const visitor = { endVisit, startVisit };
            subject.accept('C', visitor);
            expect(accept).to.be.calledTwice
                .and.to.be.calledWithExactly('C', visitor)
                .and.to.be.calledWithExactly('C', visitor);
            expect(endVisit).to.be.calledOnceWithExactly('line', subject, 'C');
            expect(startVisit).to.be.calledOnceWithExactly('line', subject, 'C');
        });
    });
});