const chai = require('chai');

const { expect } = chai;

const { Color } = require('../../../src/layout');

const subject = Color.byName();

describe('layout/Color', () => {
    describe('#byName()', () => {
        it('Should return undefined', () => {
            expect(subject.arbitraryString).to.be.undefined;
            expect(subject.value).to.be.undefined;
        });
        it('Should return the default color', () => {
            expect(subject.reset).to.be.deep.equal(['\u001b[0m']);
        });
        it('Should return a bold font', () => {
            expect(subject.bold.value).to.be.deep.equal(['\u001b[1m', '\u001b[0m']);
        });
        it('Should return a dimmed color', () => {
            expect(subject.dim.value).to.be.deep.equal(['\u001b[2m', '\u001b[0m']);
        });
        it('Should return an underlined font', () => {
            expect(subject.under.value).to.be.deep.equal(['\u001b[4m', '\u001b[0m']);
        });
        it('Should return green foreground', () => {
            expect(subject.fgGreen.value).to.be.deep.equal(['\u001b[32m', '\u001b[0m']);
        });
        it('Should return blue background', () => {
            expect(subject.bgBlue.value).to.be.deep.equal(['\u001b[44m', '\u001b[0m']);
        });
        it('Should return light green foreground', () => {
            expect(subject.fgLightGreen.value).to.be.deep.equal(['\u001b[92m', '\u001b[0m']);
        });
        it('Should return light blue background', () => {
            expect(subject.bgLightBlue.value).to.be.deep.equal(['\u001b[104m', '\u001b[0m']);
        });
        it('Should return green foreground and blue background', () => {
            expect(subject.fgGreen.bgBlue.value).to.be.deep.equal(['\u001b[32;44m', '\u001b[0m']);
        });
    });
});