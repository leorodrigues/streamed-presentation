const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { Cell } = require('../../../src/layout');

const subject = new Cell('testField', 'some-style');

const sandbox = sinon.createSandbox();

const startVisit = sandbox.stub();
const endVisit = sandbox.stub();

describe('layout/Cell', () => {
    describe('#accept', () => {
        it('Should apply the given visitor onto itself', () => {
            subject.accept('C', { endVisit, startVisit });
            expect(startVisit).to.be.calledOnceWithExactly('cell', subject, 'C');
            expect(endVisit).to.be.calledOnceWithExactly('cell', subject, 'C');
        });
    });

    describe('#pathPart', () => {
        it('Should give out the orientation', () =>
            expect(new Cell('nm').pathPart).to.be.equal('nm.cell'));
    });
});