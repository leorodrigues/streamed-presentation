const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { EventfulVisitor } = require('../../../src/layout');

const subject = new EventfulVisitor();

const sandbox = sinon.createSandbox();

const respond = sandbox.stub();

describe('layout/EventfulVisitor', () => {
    afterEach(() => sandbox.reset());

    describe('#startVisit', () => {
        it('Should emit an "enter-*" event', () => {
            subject.once('enter-fake-type', respond);
            subject.startVisit('fake-type', 'fake-element', 'fake-context');
            expect(respond).to.be.calledOnceWithExactly('fake-element', 'fake-context');
        });
    });

    describe('#endVisit', () => {
        it('Should emit an "exit-*" event', () => {
            subject.once('exit-fake-type', respond);
            subject.endVisit('fake-type', 'fake-element', 'fake-context');
            expect(respond).to.be.calledOnceWithExactly('fake-element', 'fake-context');
        });
    });

    describe('#done', () => {
        it('Should emit an "done" event', () => {
            subject.once('done', respond);
            subject.done();
            expect(respond).to.be.calledOnceWithExactly();
        });
    });
});