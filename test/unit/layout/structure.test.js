const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { Structure } = require('../../../src/layout');


const sandbox = sinon.createSandbox();

const startVisit = sandbox.stub();
const endVisit = sandbox.stub();
const accept = sandbox.stub();
const done = sandbox.stub();

describe('layout/Structure', () => {
    afterEach(() => sandbox.reset());

    describe('#accept', () => {
        it('Should apply the given visitor to it\'s elements and onto itself', () => {
            const element = { accept };
            const subject = new Structure([ element, element ]);

            subject.accept('C', { endVisit, startVisit, done });

            expect(accept).to.be.calledTwice;
            expect(startVisit).to.be.calledOnceWithExactly('structure', subject, 'C');
            expect(endVisit).to.be.calledOnceWithExactly('structure', subject, 'C');
            expect(done).to.be.calledOnceWithExactly();
        });
    });

});