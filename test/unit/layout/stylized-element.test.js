const chai = require('chai');

const { expect } = chai;

const { StylizedElement } = require('../../../src/layout');

const subject = new StylizedElement();

describe('layout/StylizedElement', () => {
    describe('#setStyle', () => {
        it('Should update it\'s own style attribute', () => {
            subject.setStyle({
                width: 'a', color: 'b', padding: 'c', fill: 'd', align: 'e'
            });
            expect(subject.style).to.be.deep.equal({
                width: 'a', color: 'b', padding: 'c', fill: 'd', align: 'e'
            });
        });

        it('Should do nothin if the given style is empty', () => {
            const subject = new StylizedElement();
            subject.setStyle({ });
            expect(subject.style).to.be.undefined;
        });
    });
});
