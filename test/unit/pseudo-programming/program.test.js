const chai = require('chai');

const { expect } = chai;

const { Program } = require('../../../src/pseudo-programming');

const subject = new Program(['x', 'y', 'z']);

describe('pseudo-programming/Program', () => {

    describe('#length', () => {
        it('Should return the operation count of the program', () => {
            expect(subject.length).to.be.equal(3);
        });
    });

    describe('#[Symbol.iterator]', () => {
        it('Should iterate through the program', () => {
            expect([...subject]).to.be.deep.equal(['x', 'y', 'z']);
        });
    });

    describe('#[integer]', () => {
        it('Should return the nth instruction', () => {
            expect(subject[1]).to.be.equal('y');
        });
    });
});