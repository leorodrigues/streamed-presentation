const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { ProgramIterator } = require('../../../src/pseudo-programming');

const sandbox = sinon.createSandbox();

const stepForward = sandbox.stub();
const newPointer = sandbox.stub();
const operation = sandbox.stub();

const pointerFactory = { newPointer };
const pointer = { endOfProgram: false, stepForward, position: 0 };

const subject = new ProgramIterator(pointerFactory);

describe('pseudo-programming/ProgramIterator', () => {
    afterEach(() => sandbox.reset());

    describe('#iterate', () => {
        it('Should iterate the contents of the program', () => {
            const e = [false, true];
            newPointer.returns(pointer);
            for (const op of subject.iterate([ operation ], 'the-context')) {
                pointer.endOfProgram = e.shift();
                op();
            }
            expect(stepForward).to.be.calledOnceWithExactly();
            expect(newPointer).to.be.calledOnceWithExactly(1);
            expect(operation).to.be.calledTwice
                .and.calledWithExactly('the-context', pointer)
                .and.calledWithExactly('the-context', pointer);
        });
    });
});