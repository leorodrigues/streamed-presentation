const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { ProgramRunner } = require('../../../src/pseudo-programming');

const sandbox = sinon.createSandbox();

const iterate = sandbox.stub();
const newContext = sandbox.stub();
const operation = sandbox.stub();

const subject = new ProgramRunner({ newContext }, { iterate });

describe('pseudo-programming/ProgramRunner', () => {
    afterEach(() => sandbox.reset());

    describe('#run', () => {
        it('Should iterate the contents of a program and execute the operations', () => {
            iterate.returns([operation]);
            newContext.returns('context');
            
            subject.run('program', 'datasource', 'dataoutput');

            expect(newContext).to.be.calledOnceWithExactly('datasource', 'dataoutput');
            expect(iterate).to.be.calledOnceWithExactly('program', 'context');
            expect(operation).to.be.calledOnceWithExactly();
        });
    });
});