const chai = require('chai');

const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { OperationSetBinder } = require('../../../src/pseudo-programming');

const sandbox = sinon.createSandbox();

const newProgram = sandbox.stub();
const someOperation = sandbox.stub();

const subject = new OperationSetBinder({ newProgram }, { someOperation });

describe('pseudo-programming/OperationSetBinder', () => {
    afterEach(() => sandbox.reset());

    describe('.INSTANCE', () => {
        it('Should return the default instance', () => {
            expect(OperationSetBinder.INSTANCE).to.be.instanceOf(OperationSetBinder);
        });
    });

    describe('#bind', () => {
        it('Should return a program instance made out of bound operations', () => {
            newProgram.returns('the new program');
            const program = subject.bind([{ op: 'someOperation', parameters: 'p' }]);
            expect(someOperation).to.be.calledOnceWithExactly('p');
            expect(program).to.be.equal('the new program');
        });
    });
});