const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { Context } = require('../../../src/pseudo-programming');

const sandbox = sinon.createSandbox();

const push = sandbox.stub();
const convert = sandbox.stub();
const getIterator = sandbox.stub();
const getVariable = sandbox.stub();

const dataSource = { getIterator, getVariable };
const dataOutput = { push };
const converter = { convert };

let subject = new Context(dataSource, dataOutput, converter);

describe('pseudo-programming/Context', () => {
    afterEach(() => sandbox.reset());

    describe('#dataExausted', () => {
        it('Should return true for a pristine instance of context', () => {
            expect(subject.dataExausted).to.be.true;
        });
    });

    describe('#pushResult', () => {
        it('Should delegate results to the output', () => {
            subject.pushResult('the-result');
            expect(push).to.be.calledOnceWithExactly('the-result');
        });
    });

    describe('#convert', () => {
        it('Should delegate the conversion to the converter, passing itself alog', () => {
            subject.convert('the-element');
            expect(convert).to.be.calledOnceWithExactly('the-element', subject);
        });
    });

    describe('#switchIterator', () => {
        it('Should request a new iterator', () => {
            getIterator.returns('iterator-instance');
            subject.switchIterator('the-iterator');
            expect(subject.dataIterator).to.be.equal('iterator-instance');
            expect(getIterator).to.be.calledOnceWithExactly('the-iterator');
        });
    });

    describe('#fetchVariable', () => {
        it('Should update the current variable', () => {
            getVariable.returns(true);
            subject = new Context(dataSource, dataOutput, converter);
            subject.fetchVariable();
            expect(subject.currentValue).to.be.true;
            expect(subject.isFalse).to.be.false;
        });

        it('Should nullify the current value if there is no such variable', () => {
            subject = new Context(dataSource, dataOutput, converter);
            subject.fetchVariable();
            expect(subject.currentValue).to.be.null;
            expect(subject.isFalse).to.be.true;
        });
    });

    describe('#fetchData', () => {
        it('Should update the current data object', () => {
            subject = new Context(dataSource, dataOutput, converter);
            subject.dataIterator = (['a'])[Symbol.iterator]();
            subject.fetchData();
            expect(subject.dataObject).to.be.equal('a');
        });

        it('Should nullify the current data object because the iterator is empty', () => {
            subject = new Context(dataSource, dataOutput, converter);
            subject.dataIterator = ([ ])[Symbol.iterator]();
            subject.fetchData();
            expect(subject.dataObject).to.be.null;
        });

        it('Should nullify the current data object because there is no current iterator', () => {
            subject = new Context(dataSource, dataOutput, converter);
            subject.dataObject = 'x';
            expect(subject.dataObject).to.be.equal('x');
            subject.dataIterator = null;
            subject.fetchData();
            expect(subject.dataObject).to.be.null;
        });
    });

    describe('#getDataField', () => {
        it('Should return undefined if there is no data object', () => {
            subject = new Context(dataSource, dataOutput, converter);
            expect(subject.getDataField('some-name')).to.be.undefined;
        });
        it('Should return undefined if the field does not exist', () => {
            subject = new Context(dataSource, dataOutput, converter);
            subject.dataObject = { 'not-the-field-name': 11 };
            expect(subject.getDataField('some-name')).to.be.undefined;
        });
        it('Should return the field value from the data object', () => {
            subject = new Context(dataSource, dataOutput, converter);
            subject.dataObject = { 'some-name': 17 };
            expect(subject.getDataField('some-name')).to.be.equal(17);
        });
    });
});
