const chai = require('chai');
const sinon = require('sinon');

const { OperationSet } = require('../../../src/pseudo-programming');

const { expect } = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();

const pushResult = sandbox.stub();
const convert = sandbox.stub();
const switchIterator = sandbox.stub();
const fetchData = sandbox.stub();
const fetchVariable = sandbox.stub();
const jumpOffset = sandbox.stub();

const context = { fetchVariable, pushResult, convert, switchIterator, fetchData };

const pointer = { jumpOffset };

const subject = OperationSet.INSTANCE;

describe('pseudo-programming/OperationSet', () => {
    describe('#emit', () => {
        it('Should return an executable "emit" operation', () => {
            convert.returns('y');
            subject['emit']({ element: 'x' })(context);
            expect(pushResult).to.be.calledOnceWithExactly('y');
            expect(convert).to.be.calledOnceWithExactly('x');
        });
    });

    describe('#switch-iterator', () => {
        it('Should return an executable "switch-iterator" operation', () => {
            subject['switch-iterator']({ name: 'x' })(context);
            expect(switchIterator).to.be.calledOnceWithExactly('x');
        });
    });

    describe('#fetch-data', () => {
        it('Should return an executable "fetch-data" operation', () => {
            subject['fetch-data']()(context);
            expect(fetchData).to.be.calledOnceWithExactly();
        });
    });

    describe('#fetch-variable', () => {
        it('Should return an executable "fetch-variable" operation', () => {
            subject['fetch-variable']({ name: 'var' })(context);
            expect(fetchVariable).to.be.calledOnceWithExactly('var');
        });
    });

    describe('#jump-exausted', () => {
        describe('Considering the returned operation as the subject', () => {
            afterEach(() => sandbox.reset());

            const op = subject['jump-exausted']({ offset: 17 });

            it('Should jump to offset if the context is out of data', () => {
                context.dataExausted = true;
                op(context, pointer);
                expect(jumpOffset).to.be.calledOnceWithExactly(17);
            });

            it('Should do nothing if there is still data left on the context', () => {
                context.dataExausted = false;
                op(context, pointer);
                expect(jumpOffset).to.not.be.called;
            });
        });

    });

    describe('#jump-false', () => {
        describe('Considering the returned operation as the subject', () => {
            afterEach(() => sandbox.reset());

            const op = subject['jump-false']({ offset: 17 });

            it('Should jump to offset if the current value is falsy', () => {
                context.isFalse = true;
                op(context, pointer);
                expect(jumpOffset).to.be.calledOnceWithExactly(17);
            });

            it('Should do nothing if the current value if truthy', () => {
                context.isFalse = false;
                op(context, pointer);
                expect(jumpOffset).to.not.be.called;
            });
        });

    });

    describe('#jump-offset', () => {
        it('Should return an executable "jump-offset" operation', () => {
            subject['jump-offset']({ offset: 23 })(undefined, pointer);
            expect(jumpOffset).to.be.calledOnceWithExactly(23);
        });
    });
});