const chai = require('chai');

const { expect } = chai;

const { ExecutionPointer, ProgramJumpError } =
    require('../../../src/pseudo-programming');

const subject = new ExecutionPointer(13);

describe('pseudo-programming/ExecutionPointer', () => {
    afterEach(() => subject.position = 0);

    describe('#endOfProgram', () => {
        it('Should return true if the current position is after the last instruction', () => {
            subject.position = 13;
            expect(subject.endOfProgram).to.be.true;
        });

        it('Should be false if the current position is before the last instruction', () => {
            subject.position = 7;
            expect(subject.endOfProgram).to.be.false;
        });

        it('Should be false if the current position is at the last instruction', () => {
            subject.position = 12;
            expect(subject.endOfProgram).to.be.false;
        });
    });

    describe('#stepForward', () => {
        it('Should move the pointer forward by one position', () => {
            subject.position = 7;
            expect(subject.stepForward()).to.be.equal(7);
            expect(subject.position).to.be.equal(8);
        });
    });

    describe('#jumpOffset', () => {
        it('Should move the pointer forward by an arbitrary amount', () => {
            subject.position = 3;
            expect(subject.jumpOffset(8)).to.be.equal(3);
            expect(subject.position).to.be.equal(11);
        });
    });

    describe('#confirmDestinationIsWithinBounds', () => {
        it('Should pass through if destination falls within [0:length of the program]', () => {
            subject.confirmDestinationIsWithinBounds(7);
        });

        it('Should pass through if destination falls at the start of program', () => {
            subject.confirmDestinationIsWithinBounds(0);
        });

        it('Should pass through if destination falls at the end of program', () => {
            subject.confirmDestinationIsWithinBounds(13);
        });

        it('Should throw an exception if destination is before the start of program', () => {
            try {
                subject.confirmDestinationIsWithinBounds(-2);

                /* istanbul ignore next */
                throw new Error('Execution should not reach this point');
            } catch(error) {
                expect(error).to.be.instanceOf(ProgramJumpError);
            }
        });

        it('Should throw an exception if destination is after the end of program', () => {
            try {
                subject.confirmDestinationIsWithinBounds(14);

                /* istanbul ignore next */
                throw new Error('Execution should not reach this point');
            } catch(error) {
                expect(error).to.be.instanceOf(ProgramJumpError);
            }
        });
    });
});