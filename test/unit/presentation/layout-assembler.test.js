const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const sandbox = sinon.createSandbox();

const accept = sandbox.stub();
const makeDriver = sandbox.stub();
const makeContext = sandbox.stub();
const newStructure = sandbox.stub();
const newAppearance = sandbox.stub();
const primePseudoCode = sandbox.stub();
const newRealizeStructure = sandbox.stub();

const { LayoutAssembler } = require('../../../src/presentation');

const subject = new LayoutAssembler({
    newStructure,
    newAppearance,
    newRealizeStructure
});

describe('presentation/LayoutAssembler', () => {
    describe('#assemble', () => {
        it('Should return an array of visitors', () => {
            makeContext.returns('ctx');
            primePseudoCode.returns('the code');
            newStructure.returns({ accept, primeCode: () => primePseudoCode });
            newAppearance.returns('appearance');

            newRealizeStructure.returns({ makeDriver, makeContext });
            makeDriver.returns('layout-driver');

            expect(subject.assemble('s-data', 'a-data', 'op')).to.be.deep.equal('the code');

            expect(makeContext).to.be.calledOnceWithExactly('appearance');
            expect(newRealizeStructure).to.be.calledOnceWithExactly();
            expect(newAppearance).to.be.calledOnceWithExactly('a-data');
            expect(newStructure).to.be.calledOnceWithExactly('s-data');
            expect(accept).to.be.calledOnceWithExactly('ctx', 'layout-driver');
        });
    });
});
