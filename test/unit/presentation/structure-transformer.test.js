const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { StructureTransformer } = require('../../../src/presentation');

const sandbox = sinon.createSandbox();
const newStructure = sandbox.stub();
const newLabel = sandbox.stub();
const newLine = sandbox.stub();
const newCell = sandbox.stub();
const newVenetian = sandbox.stub();
const newVenetianBlade = sandbox.stub();

const subject = new StructureTransformer({
    newStructure, newLabel, newLine, newCell,
    newVenetian, newVenetianBlade
});

describe('presentation/StructureTransformer', () => {
    afterEach(() => sandbox.reset());

    it('Should collect the children of a structure', () => {
        const driver = subject.makeDriver();
        const children = [ ];
        driver.emit('enter-structure', { contents: [ 1, 1, 2, 3, 5, 8 ] }, { children });
        driver.emit('done');
        expect(children).to.be.deep.equal([ 1, 1, 2, 3, 5, 8 ]);
    });

    it('Should collect the children of a Venetian', () => {
        const driver = subject.makeDriver();
        const children = [ ];
        driver.emit('enter-venetian', { blades: [ { f: 'x' } ] }, { children });
        driver.emit('done');
        expect(children).to.be.deep.equal([ { type: 'blade', f: 'x' }]);
    });

    it('Should collect the children of a Blade', () => {
        const driver = subject.makeDriver();
        const children = [ ];
        driver.emit('enter-blade', { contents: [ 1, 2 ] }, { children });
        driver.emit('done');
        expect(children).to.be.deep.equal([ 1, 2 ]);
    });

    it('Should collect the children of a line', () => {
        const driver = subject.makeDriver();
        const children = [ ];
        driver.emit('enter-line', { contents: [ 1, 2, 3, 5, 7, 11 ] }, { children });
        driver.emit('done');
        expect(children).to.be.deep.equal([ 1, 2, 3, 5, 7, 11 ]);
    });

    it('Should fabricate a structure', () => {
        newStructure.returns('S');
        const driver = subject.makeDriver();
        const ctx = { parameters: ['a', 'b'] };
        driver.emit('exit-structure', {  }, ctx);
        driver.emit('done');
        expect(newStructure).to.be.calledOnceWithExactly([ 'a', 'b' ]);
        expect(ctx.product).to.be.equal('S');
    });

    it('Should fabricate a venetian', () => {
        newVenetian.returns('V');
        const driver = subject.makeDriver();
        const ctx = { parameters: ['a', 'b'] };
        driver.emit('exit-venetian', { 'opened-when': 'now', orientation: 'o' }, ctx);
        driver.emit('done');
        expect(newVenetian).to.be.calledOnceWithExactly('now', [ 'a', 'b' ], 'o');
        expect(ctx.product).to.be.equal('V');
    });

    it('Should fabricate a blade', () => {
        newVenetianBlade.returns('B');
        const driver = subject.makeDriver();
        const ctx = { parameters: ['a', 'b'] };
        driver.emit('exit-blade', { when: 'open' }, ctx);
        driver.emit('done');
        expect(newVenetianBlade).to.be.calledOnceWithExactly([ 'a', 'b' ], 'open');
        expect(ctx.product).to.be.equal('B');
    });

    it('Should fabricate a line', () => {
        newLine.returns('L');
        const driver = subject.makeDriver();
        const ctx = { parameters: ['k', 'o'] };
        driver.emit('exit-line', { iterator: 'it' }, ctx);
        driver.emit('done');
        expect(newLine).to.be.calledOnceWithExactly('it', [ 'k', 'o' ]);
        expect(ctx.product).to.be.equal('L');
    });

    it('Should fabricate a label', () => {
        newLabel.returns('B');
        const driver = subject.makeDriver();
        const ctx = {  };
        driver.emit('exit-label', { name: 'N', value: 'V' }, ctx);
        driver.emit('done');
        expect(newLabel).to.be.calledOnceWithExactly('N', 'V');
        expect(ctx.product).to.be.equal('B');
    });

    it('Should fabricate a cell', () => {
        newCell.returns('C');
        const driver = subject.makeDriver();
        const ctx = {  };
        driver.emit('exit-cell', { name: 'M' }, ctx);
        driver.emit('done');
        expect(newCell).to.be.calledOnceWithExactly('M');
        expect(ctx.product).to.be.equal('C');
    });
});
