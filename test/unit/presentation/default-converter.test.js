const chai = require('chai');
const sinon = require('sinon');
chai.use(require('sinon-chai'));

const { expect } = chai;

const { DefaultConverter } = require('../../../src/presentation');
const { ConversionError } = require('../../../src/pseudo-programming');

const subject = new DefaultConverter();

const sandbox = sinon.createSandbox();
const context = { getDataField: sandbox.stub() };

describe('presentation/DefaultConverter', () => {
    afterEach(() => sandbox.reset());

    describe('#convert', () => {
        it('Should convert a "break"', () => {
            const elmnt = { type: 'break' };
            expect(subject.convert(elmnt, context)).to.be.deep.equal({ type: 'break' });
        });
        it('Should convert a "label" w/o value', () => {
            context.getDataField.returns('Y');
            const elmnt = { type: 'label', name: 'e', style: { color: 'c' } };
            const result = subject.convert(elmnt, context);
            expect(result).to.be.deep.equal({ type: 'label', text: 'Y', color: 'c' });
            expect(context.getDataField).to.be.calledOnceWithExactly('e');
        });
        it('Should convert a "label" w/o value or field', () => {
            const elmnt = { type: 'label', name: 'e', style: { color: 'c' } };
            const result = subject.convert(elmnt, context);
            expect(result).to.be.deep.equal({ type: 'label', text: 'E', color: 'c' });
            expect(context.getDataField).to.be.calledOnceWithExactly('e');
        });
        it('Should convert a "label" with value', () => {
            const elmnt = { type: 'label', name: 'e', style: { color: 'c' }, value: 'v' };
            const result = subject.convert(elmnt, context);
            expect(context.getDataField).not.to.be.called;
            expect(result).to.be.deep.equal({ type: 'label', text: 'v', color: 'c' });
        });
        it('Should convert a "cell" w/o field', () => {
            const elmnt = { type: 'cell', name: 'e', style: { color: 'c' } };
            const result = subject.convert(elmnt, context);
            expect(result).to.be.deep.equal({ type: 'cell', text: '', color: 'c' });
            expect(context.getDataField).to.be.calledOnceWithExactly('e');
        });
        it('Should convert a "cell" w/ field', () => {
            context.getDataField.returns('Z');
            const elmnt = { type: 'cell', name: 'e', style: { color: 'c' } };
            const result = subject.convert(elmnt, context);
            expect(result).to.be.deep.equal({ type: 'cell', text: 'Z', color: 'c' });
            expect(context.getDataField).to.be.calledOnceWithExactly('e');
        });
        it('Should throw if type is not recognized', () => {
            try {
                subject.convert({ type: 'fake-type' });

                /* istanbul ignore next */
                throw new Error('Excecution should not reach this point');
            } catch(error) {
                expect(error).to.be.instanceOf(ConversionError);
            }
        });
    });
});
