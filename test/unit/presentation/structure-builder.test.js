const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { StructureBuilder } = require('../../../src/presentation');


const sandbox = sinon.createSandbox();

const startVisit = sandbox.stub();
const endVisit = sandbox.stub();
const fakeDone = sandbox.stub();

const children = [{ type: 'm' }];

const fakeDriver = {
    startVisit(type, element, context) {
        children.length && context.children.push(children.pop());
        startVisit(type, element);
    },

    endVisit(type, element, context) {
        context.product = 'P';
        endVisit(type, element);
    },

    done: fakeDone

};

describe('presentation/StructureBuilder', () => {
    afterEach(() => sandbox.reset());

    describe('#buildFromData', () => {
        it('Should follow the data given on input', () => {
            const transformer = { makeDriver: () => fakeDriver };

            const subject = new StructureBuilder(transformer);

            const result = subject.buildFromData({ type: 'x', data: 'y' });

            expect(result).to.be.equal('P');
            expect(startVisit).to.be.calledTwice
                .and.calledWithExactly('x', { data: 'y' })
                .and.calledWithExactly('m', { });
            expect(endVisit).to.be.calledTwice
                .and.calledWithExactly('x', { data: 'y' })
                .and.calledWithExactly('m', { });
            expect(fakeDone).to.be.calledOnceWithExactly();
        });
    });

});