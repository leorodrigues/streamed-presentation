
const { PassThrough, Writable } = require('stream');

const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const sandbox = sinon.createSandbox();

const push = sandbox.stub();

const { FragmentRenderer } = require('../../../src/presentation');

const subject = new FragmentRenderer({ push });

describe('presentation/FragmentRenderer', () => {
    describe('#stringify', () => {
        it('Should stream out a stringified representation of an object', () => {
            const objects = [ ];
            const stream = makeReadable();
            stream.pipe(subject.stringify()).pipe(makeWritable(objects));
            stream.push({ message: 'hello' });

            expect(objects).to.be.deep.equal(['{"message":"hello"}\n']);
        });
    });

    describe('#render', () =>{
        it('Should render "break" fragment out into a stream', () => {
            const objects = [ ];
            const stream = makeReadable();
            stream.pipe(subject.render()).pipe(makeWritable(objects));
            stream.push({ type: 'break' });

            expect(objects).to.be.deep.equal(['\n']);
        });

        it('Should render fragment with text, out into a stream', () => {
            const objects = [ ];
            const stream = makeReadable();
            stream.pipe(subject.render()).pipe(makeWritable(objects));
            stream.push({ text: 'hello' });

            expect(objects).to.be.deep.equal(['hello ']);
        });

        it('Should render fragment with color, out into a stream', () => {
            const objects = [ ];
            const stream = makeReadable();
            stream.pipe(subject.render()).pipe(makeWritable(objects));
            stream.push({ color: ['x', 'y'] });

            expect(objects).to.be.deep.equal(['x y']);
        });

        it('Should render fragment with alignment, out into a stream', () => {
            const objects = [ ];
            const stream = makeReadable();
            stream.pipe(subject.render()).pipe(makeWritable(objects));
            stream.push({ text: 'ok', width: 10, align: 'right' });

            expect(objects).to.be.deep.equal(['        ok ']);
        });
    });
});

function makeWritable(holder) {
    return new Writable({
        objectMode: true,
        write(chunk, enc, cb) {
            holder.push(chunk.toString());
            cb();
        }
    });
}

function makeReadable() {
    return new PassThrough({
        objectMode: true
    });
}