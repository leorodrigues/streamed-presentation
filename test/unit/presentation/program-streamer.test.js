const { Readable, Writable } = require('stream');
const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const sandbox = sinon.createSandbox();

const render = sandbox.stub();
const run = sandbox.spy(fakeRun);

const { ProgramStreamer } = require('../../../src/presentation');

const subject = new ProgramStreamer({ run }, { render });

describe('presentation/ProgramStreamer', () => {
    describe('#stream', () => {
        it('Should pipe a readable stream through a program runner', async () => {
            const output = [ ];
            const renderStream = makeWritable(output);
            render.returns(renderStream);

            const result = await (new Promise(resolve =>
                resolve(subject.stream('program', 'data-source'))));

            expect(result).to.be.equal(renderStream);
            expect(output).to.be.deep.equal(['fragment']);
            expect(render).to.be.calledOnceWithExactly();
            expect(run).to.be.calledOnceWithExactly('program', 'data-source', sinon.match(o =>
                o instanceof Readable));
        });
    });
});

function fakeRun(program, dataSource, stream) {
    stream.push('fragment');
    stream.push(null);
}

function makeWritable(output) {
    return new Writable({
        objectMode: true,
        write(chunk, env, cb) {
            output.push(chunk);
            cb();
        }
    });
}
