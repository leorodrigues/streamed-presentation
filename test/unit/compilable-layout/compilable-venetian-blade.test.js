const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { CompilableVenetianBlade } = require('../../../src/compilable-layout');

const MockupKit = require('./mockup-kit');
const mockupKit = new MockupKit(sinon.createSandbox());

const element = mockupKit.makeElement();

describe('compilable-layout/CompilableVenetianBlade', () => {
    describe('#primeCode', () => {
        it('Should prime the code fabrication of it\'s children', () => {
            new CompilableVenetianBlade([ element, element ]).primeCode('c');

            expect(element.primeCode).to.be.calledTwice
                .and.calledWithExactly('c')
                .and.calledWithExactly('c');
        });
    });
});
