const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { CompilableLabel } = require('../../../src/compilable-layout');

const MockupKit = require('./mockup-kit');
const mockupKit = new MockupKit(sinon.createSandbox());

const context = mockupKit.makeContext();
const factory = mockupKit.makePseudocodeFactory();

describe('compilable-layout/CompilableLabel', () => {
    afterEach(() => mockupKit.reset());
    beforeEach(() => factory.primeEmitElement.returns(112358));

    describe('#primeCode', () => {
        it('Should yield a full pseudo "emit" code', () => {
            new CompilableLabel('test', 'value', 'style', factory).primeCode(context);

            expect(context.push).to.be.calledOnceWithExactly(112358);
            expect(factory.primeEmitElement).to.be.calledOnceWithExactly({
                type: 'label', name: 'test', style: 'style', value: 'value'
            });
        });

        it('Should yield a pseudo "emit" code w/o style', () => {
            new CompilableLabel('test', 'value', undefined, factory).primeCode(context);

            expect(context.push).to.be.calledOnceWithExactly(112358);
            expect(factory.primeEmitElement).to.be.calledOnceWithExactly({
                type: 'label', name: 'test', value: 'value'
            });
        });

        it('Should yield a pseudo "emit" code w/o style and value', () => {
            new CompilableLabel('test', undefined, undefined, factory).primeCode(context);

            expect(context.push).to.be.calledOnceWithExactly(112358);
            expect(factory.primeEmitElement).to.be.calledOnceWithExactly({
                type: 'label', name: 'test'
            });
        });
    });
});