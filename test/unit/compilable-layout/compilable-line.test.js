const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { CompilableLine } = require('../../../src/compilable-layout');

const MockupKit = require('./mockup-kit');
const mockupKit = new MockupKit(sinon.createSandbox());

const factory = mockupKit.makePseudocodeFactory();
const element = mockupKit.makeElement();
const context = mockupKit.makeContext();
const label = mockupKit.makeLabel();

describe('compilable-layout/CompilableLine', () => {
    afterEach(() => mockupKit.reset());

    describe('#primeCode', () => {
        it('Should yield just a "new line" if no iterator is given', () => {
            factory.primeEmitLineBreak.returns('newline');

            new CompilableLine(undefined, [ element ], factory).primeCode(context);

            expect(element.primeCode).to.be.calledOnceWithExactly(context);
            expect(context.push).to.be.calledOnceWithExactly('newline');
            expect(factory.primeFetchData).to.not.be.called;
            expect(factory.primeJumpOffset).to.not.be.called;
            expect(factory.primeJumpExausted).to.not.be.called;
            expect(factory.primeSwitchIterator).to.not.be.called;
            expect(factory.primeEmitLineBreak).to.be.calledOnceWithExactly();
        });

        it('Should yield a full pseudo code sequence if an iterator is given', () => {
            context.newLabel.returns(label);
            context.newReverseLabel.returns(label);

            factory.primeSwitchIterator.returns(3);
            factory.primeFetchData.returns(5);
            factory.primeJumpExausted.returns(7);
            factory.primeEmitLineBreak.returns(11);
            factory.primeJumpOffset.returns(13);

            new CompilableLine('it', [ element ], factory).primeCode(context);

            expect(context.push).to.have.callCount(5)
                .and.to.be.calledWithExactly(3)
                .and.calledWithExactly(5)
                .and.calledWithExactly(7)
                .and.calledWithExactly(11)
                .and.calledWithExactly(13);

            expect(element.primeCode)
                .to.be.calledOnceWithExactly(context);
            expect(label.markStart).to.be.calledTwice;
            expect(label.markStop).to.be.calledTwice
                .and.calledWithExactly()
                .and.calledWithExactly(2);
            expect(context.newLabel).to.be.calledOnce;
            expect(context.newReverseLabel).to.be.calledOnce;
            expect(factory.primeFetchData).to.be.calledOnceWithExactly();
            expect(factory.primeJumpOffset).to.be.calledOnceWithExactly(label);
            expect(factory.primeJumpExausted).to.be.calledOnceWithExactly(label);
            expect(factory.primeSwitchIterator).to.be.calledOnceWithExactly('it');
            expect(factory.primeEmitLineBreak).to.be.calledOnceWithExactly();
        });
    });
});