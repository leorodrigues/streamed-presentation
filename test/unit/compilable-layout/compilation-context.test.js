const chai = require('chai');
const { expect } = chai;
const { CompilationContext } = require('../../../src/compilable-layout');

describe('compilable-layout/CompilationContext', () => {
    describe('.PseudocontextLabel', () => {
        describe('#markStart', () => {
            it('Should take the pseudocode length as the start position', () => {
                const subject = new CompilationContext.PseudocodeLabel([1, 2, 3]);
                subject.markStart();
                expect(subject).to.be.deep.equal({
                    direction: CompilationContext.PseudocodeLabel.FORWARD,
                    pseudocode: [ 1, 2, 3 ],
                    start: 3
                });
            });
        });

        describe('#markStop', () => {
            it('Should take the pseudocode length as the stop position plus an offset', () => {
                const subject = new CompilationContext.PseudocodeLabel([9, 7]);
                subject.markStop(7);
                expect(subject).to.be.deep.equal({
                    direction: CompilationContext.PseudocodeLabel.FORWARD,
                    pseudocode: [ 9, 7 ],
                    stop: 9
                });
            });
        });

        describe('#offset', () => {
            it('Should return the segment length', () => {
                const pseudocode = [ 17 ];
                const subject = new CompilationContext.PseudocodeLabel(
                    pseudocode, CompilationContext.PseudocodeLabel.REVERSE);
                subject.markStart();
                pseudocode.push(19, 23);
                subject.markStop(3);
                expect(subject.offset).to.be.equal(-5);
            });
        });
    });
});