const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { CompilableCell } = require('../../../src/compilable-layout');

const MockupKit = require('./mockup-kit');
const mockupKit = new MockupKit(sinon.createSandbox());

const factory = mockupKit.makePseudocodeFactory();
const context = mockupKit.makeContext();

describe('compilable-layout/CompilableCell', () => {
    afterEach(() => mockupKit.reset());
    beforeEach(() => factory.primeEmitElement.returns(112358));

    describe('#primeCode', () => {
        it('Should yield a pseudo "emit" code with style', () => {
            new CompilableCell('test', 'style', factory).primeCode(context);

            expect(context.push).to.be.calledOnceWithExactly(112358);
            expect(factory.primeEmitElement).to.be.calledOnceWithExactly({
                type: 'cell', name: 'test', style: 'style'
            });
        });

        it('Should yield a pseudo "emit" code without style', () => {
            new CompilableCell('test', undefined, factory).primeCode(context);

            expect(context.push).to.be.calledOnceWithExactly(112358);
            expect(factory.primeEmitElement).to.be.calledOnceWithExactly({
                type: 'cell', name: 'test'
            });
        });
    });
});