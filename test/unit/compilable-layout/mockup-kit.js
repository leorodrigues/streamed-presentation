
class MockupKit {
    constructor(sandbox) {
        this.sandbox = sandbox;
    }

    makeElement() {
        return { primeCode: this.sandbox.stub() };
    }

    makePseudocodeFactory() {
        return {
            primeEmitElement: this.sandbox.stub(),
            primeFetchVariable: this.sandbox.stub(),
            primeJumpOffset: this.sandbox.stub(),
            primeJumpFalse: this.sandbox.stub(),
            primeSwitchIterator: this.sandbox.stub(),
            primeEmitLineBreak: this.sandbox.stub(),
            primeJumpExausted: this.sandbox.stub(),
            primeFetchData: this.sandbox.stub(),
            primeCode: this.sandbox.stub(),
            newContext: this.sandbox.stub()
        };
    }

    makeContext() {
        const context = [ this.sandbox.stub() ];
        context.newLabel = this.sandbox.stub();
        context.newReverseLabel = this.sandbox.stub();
        this.sandbox.spy(context, 'push');
        return context;
    }

    makeLabel(offset) {
        return {
            offset,
            markStart: this.sandbox.stub(),
            markStop: this.sandbox.stub()
        };
    }

    makeBlade(condition) {
        const primeCode = this.sandbox.stub();
        return { primeCode, condition };
    }

    reset() {
        this.sandbox.reset();
    }
}

module.exports = MockupKit;