const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { CompilableStructure } = require('../../../src/compilable-layout');

const MockupKit = require('./mockup-kit');
const mockupKit = new MockupKit(sinon.createSandbox());

const context = mockupKit.makeContext();
const element = mockupKit.makeElement();

describe('compilable-layout/CompilableStructure', () => {
    afterEach(() => mockupKit.reset());
    describe('#primeCode', () => {
        it('Should yield a pseudo "emit" code with style', () => {
            context[0].returns(17);
            const subject = new CompilableStructure([ element ]);
            const primedCode = subject.primeCode(context);
            expect(primedCode()).to.be.deep.equal([ 17 ]);
            expect(element.primeCode).to.be.calledOnceWithExactly(context);
            expect(context[0]).to.be.calledOnceWithExactly();
        });
    });
});