const chai = require('chai');

const { expect } = chai;

const { PseudoCodeFactory } = require('../../../src/compilable-layout');

const subject = new PseudoCodeFactory();

describe('compilable-layout/PseudoCodeFactory', () => {
    describe('#primeFetchData', () => {
        it('Should create a pseudo "fetch-data" code', () => {
            const fireCodeFabrication = subject.primeFetchData();
            expect(fireCodeFabrication()).to.be.deep.equal({ op: 'fetch-data' });
        });
    });

    describe('#primeFetchVariable', () => {
        it('Should create a pseudo "fetch-variable" code', () => {
            const fireCodeFabrication = subject.primeFetchVariable('test');
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'fetch-variable', parameters: { name: 'test' }
            });
        });
    });

    describe('#primeEmitLineBreak', () => {
        it('Should create a pseudo "emit" code for line break', () => {
            const fireCodeFabrication = subject.primeEmitLineBreak();
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'emit', parameters: { element: { type: 'break' } }
            });
        });
    });

    describe('#primeEmitElement', () => {
        it('Should create a pseudo "emit" code', () => {
            const fireCodeFabrication = subject.primeEmitElement('test');
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'emit', parameters: { element: 'test' }
            });
        });
    });

    describe('#primeJumpExausted', () => {
        it('Should create a pseudo "jump-exausted" code', () => {
            const codeLabel = { offset: 42 };
            const fireCodeFabrication = subject.primeJumpExausted(codeLabel);
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'jump-exausted', parameters: { offset: 42 }
            });
        });
    });

    describe('#primeJumpFalse', () => {
        it('Should create a pseudo "jump-false" code', () => {
            const codeLabel = { offset: 42 };
            const fireCodeFabrication = subject.primeJumpFalse(codeLabel);
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'jump-false', parameters: { offset: 42 }
            });
        });
    });

    describe('#primeJumpOffset', () => {
        it('Should create a pseudo "jump-exausted" code', () => {
            const codeLabel = { offset: 11 };
            const fireCodeFabrication = subject.primeJumpOffset(codeLabel);
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'jump-offset', parameters: { offset: 11 }
            });
        });
    });

    describe('#primeSwitchIterator', () => {
        it('Should create a pseudo "switch-iterator" code', () => {
            const fireCodeFabrication = subject.primeSwitchIterator('refrigerator');
            expect(fireCodeFabrication()).to.be.deep.equal({
                op: 'switch-iterator', parameters: { name: 'refrigerator' }
            });
        });
    });
});
