const chai = require('chai');
const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const { CompilableVenetian, CompilableVenetianBlade } =
    require('../../../src/compilable-layout');

const MockupKit = require('./mockup-kit');
const mockupKit = new MockupKit(sinon.createSandbox());

const factory = mockupKit.makePseudocodeFactory();
const context = mockupKit.makeContext();
const label = mockupKit.makeLabel(117);

const openedBlade = mockupKit.makeBlade(
    CompilableVenetianBlade.CONDITION_OPENED);

const closedBlade = mockupKit.makeBlade(
    CompilableVenetianBlade.CONDITION_CLOSED);

describe('compilable-layout/CompilableVenetian', () => {
    afterEach(() => mockupKit.reset());

    describe('Having opened blades only', () => {
        describe('#primeCode', () => {
            it('Should prime the code fabrication', () => {
                factory.primeFetchVariable.returns(2);
                factory.primeJumpFalse.returns(3);

                context.newLabel.returns(label);

                new CompilableVenetian('sw', [ openedBlade ], 'o', factory).primeCode(context);

                expect(openedBlade.primeCode).to.be.calledOnceWithExactly(context);
                expect(context.newLabel).to.be.calledOnceWithExactly();
                expect(factory.primeFetchVariable).to.be.calledOnceWithExactly('sw');
                expect(factory.primeJumpFalse).to.be.calledOnceWithExactly(label);
            });
        });
    });

    describe('Having both opened and closed blades', () => {
        describe('#primeCode', () => {
            it('Should prime the code fabrication', () => {
                const fakeBlades = [ openedBlade, closedBlade ];

                context.newLabel.returns(label);

                factory.primeFetchVariable.returns(3);
                factory.primeJumpFalse.returns(5);
                factory.primeJumpOffset.returns(7);

                new CompilableVenetian('sw', fakeBlades, 'o', factory).primeCode(context);

                expect(openedBlade.primeCode).to.be.calledOnceWithExactly(context);
                expect(closedBlade.primeCode).to.be.calledOnceWithExactly(context);
                expect(factory.primeFetchVariable).to.be.calledOnceWithExactly('sw');
                expect(factory.primeJumpFalse).to.be.calledOnceWithExactly(label);
                expect(context.newLabel).to.be.calledTwice
                    .and.calledWithExactly()
                    .and.calledWithExactly();
            });
        });
    });
});
